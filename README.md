# Gobii Connector

A universal API + SDK to easily load Gobii jobs onto servers with update streams.

### Dependencies

* RabbitMQ Installation

### Installation

1. Run maven to install all dependencies
2. Run project through IDE (no JAR as of this time)

## Components

1. **Gobii Processing API** - Offers a POST endpoint to send parameters to a gobii job.
2. **Gobii Javascript SDK** - Easy to use commands from any web based / hybrid based application to connect to Gobii API.

## Usage

### Gobii Processing API

`POST` `<SERVER_URL>/v1/job` - Create gobii load and get update stream ID

```
Parameters: <Any>
{
    // All key value pairs go to gobii invokation
}
```

`WSS` `wss://<SERVER_URL>/ping/<Queue_id>` - Websocket ping point

`WSS` `wss://<SERVER_URL>/queue/<Queue_id>` - Websocket update listener


### Gobii Javascript SDK

To simplify connection, and have all data stream to be managed, we recommend using the javascript SDK

**Installation**

* Import JS file from the server `<script src="<SERVER_URL>/stream.js"></script>`

**Usage**

There will be an available global variable `gobii`.

* `gobii.load(params, callback)` - loads a new gobii job with arguments
  * `params`: parameters to pass to gobii job in JSON format
  * `callback`: Callback function for update listener. This function will run whenever an update of the gobii process has been received.
* Other methods of `gobii` is available but everything is already managed under the `load` method.

## TODO

*  API: Invoke a gobii JAR and accept a `queueId` to be able to push updates to
*  SDK: Offer feature to get job updates from loaded items (not from creating of jobs through `load`)