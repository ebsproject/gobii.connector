package com.ebs.gobiiconnector.Test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class TestController {
    @RequestMapping("test")
    public String welcome() {
        return "index.html";
    }
}
