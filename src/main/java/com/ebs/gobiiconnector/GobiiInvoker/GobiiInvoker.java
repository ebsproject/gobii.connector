package com.ebs.gobiiconnector.GobiiInvoker;

import com.ebs.gobiiconnector.MQHandler.MQConfig;
import com.ebs.gobiiconnector.MQHandler.MQHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GobiiInvoker {
    public String status = "Processing";

    public void runLoader(String payload) {
        try {
            JSONObject pJson = new JSONObject(payload);
            String file = pJson.getString("filepath");
            String[] fpArr = file.split("\\.", 0);
            String filetype = fpArr[fpArr.length-1];

            Thread pipeInvoker = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Process process = new ProcessBuilder(MQConfig.GBLOADERPATH,file,filetype).start();
                        status = "Done";
                        System.out.println(this.getShellResult(process));
                    } catch(IOException e) {
                        e.printStackTrace();
                    }
                }

                private String getShellResult(Process p) throws IOException {
                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(p.getInputStream()));
                    StringBuilder builder = new StringBuilder();
                    String line = null;
                    while ( (line = reader.readLine()) != null) {
                        builder.append(line);
                        builder.append(System.getProperty("line.separator"));
                    }
                    return builder.toString();
                }
            });
            pipeInvoker.start();
        } catch (JSONException err) {
            err.printStackTrace();
        }
    }
}
