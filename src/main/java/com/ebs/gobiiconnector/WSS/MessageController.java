package com.ebs.gobiiconnector.WSS;

import com.ebs.gobiiconnector.MQHandler.MQConfig;
import com.rabbitmq.client.ShutdownSignalException;
import com.rabbitmq.tools.json.JSONUtil;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.amqp.AmqpIOException;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController {
    @MessageMapping("/{queueId}")
    @SendTo("/queue/{queueId}")
    public Message send(String msg, @DestinationVariable String queueId) throws Exception {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(new CachingConnectionFactory(MQConfig.MQHOST));
        boolean kill = false;
        try {
            String payload = new String(rabbitTemplate.receive(queueId).getBody());
            return new Message(payload);
        } catch (NullPointerException | ShutdownSignalException | AmqpIOException e) {
            kill = true;
        }

        if(kill)
            return new Message("{\"progress\": \"done\"}");
        return new Message("wait");
    }
}
