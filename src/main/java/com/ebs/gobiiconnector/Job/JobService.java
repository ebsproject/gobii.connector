package com.ebs.gobiiconnector.Job;

import com.ebs.gobiiconnector.GobiiInvoker.GobiiInvoker;
import com.ebs.gobiiconnector.MQHandler.MQConfig;
import com.ebs.gobiiconnector.MQHandler.MQHandler;
import com.rabbitmq.tools.json.JSONUtil;
import org.apache.tomcat.util.json.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Random;

@Service
public class JobService {

    @Autowired
    private MQHandler mqHandler;

    @Autowired
    private AmqpAdmin admin;

    // This method is for creating a new load
    public List<String> invokeJob(String payload) {
        // create queue and get queue id
        String queueId = this.generateId();
        mqHandler.createQueue(queueId);

        // invoke Gobii and pass queue id to push updates to
        GobiiInvoker ginvoke = new GobiiInvoker();
        ginvoke.runLoader(payload);

        Thread counter = new Thread(new Runnable() {
            @Override
            public void run() {
                RabbitTemplate rabbitTemplate = new RabbitTemplate(new CachingConnectionFactory(MQConfig.MQHOST));
                boolean run = true;
                int count = 10;
                while(run) {
                    if(ginvoke.status.equals("Done"))
                        run = false;
                    rabbitTemplate.convertAndSend(queueId, "{\"progress\": "+ginvoke.status+"}");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                admin.deleteQueue(queueId);
            }
        });
        counter.start();
        // end dummy

        // return wss url = wss://{server_address}/subscribe/{queue_id}
        String baseUrl =
                ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        baseUrl = baseUrl.replace("http://", "");
        baseUrl = baseUrl.replace("https://", "");
        return List.of("wss://"+baseUrl+"/"+queueId);
    }

    public String generateId() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    }

    private class Payload {
        String url;
        protected Payload(String url) {
            this.url = url;
        }
    }

}
