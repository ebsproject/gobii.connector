package com.ebs.gobiiconnector.MQHandler;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MQConfig {

    public static String MQHOST = "test";
    public static String GBLOADERPATH = "";

    static final String topicExchangeName = "gobiijobs";
    private String queueName = "test";
    private Queue currentQueue;

    @Bean
    CachingConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(MQHOST);
    }

    @Bean
    RabbitAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    RabbitTemplate rabbitTemplate() {
        return new RabbitTemplate(connectionFactory());
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }
}
