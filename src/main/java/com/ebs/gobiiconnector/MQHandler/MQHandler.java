package com.ebs.gobiiconnector.MQHandler;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MQHandler {

    @Autowired
    private AmqpAdmin admin;

    public MQHandler() {

    }

    public Queue createQueue(String queueName) {
        Queue queue = new Queue(queueName, true, false, false);
        TopicExchange te = new TopicExchange("gobiijobs");
        Binding binding = this.createBinding(queue, te, queueName);
        admin.declareQueue(queue);
        admin.declareBinding(binding);
        return queue;
    }

    private Binding createBinding(Queue queue, TopicExchange exchange, String qid) {
        return BindingBuilder.bind(queue).to(exchange).with("gobii.jobs."+qid);
    }

}
