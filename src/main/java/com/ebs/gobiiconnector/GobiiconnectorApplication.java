package com.ebs.gobiiconnector;

import com.ebs.gobiiconnector.MQHandler.MQConfig;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class GobiiconnectorApplication {

	public static void main(String[] args) {
		JSONObject config = parseConfig(args[0]);
		try {
			MQConfig.MQHOST = config.getString("RMQHOST");
			MQConfig.GBLOADERPATH = config.getString("GBLOADERPATH");
		} catch (JSONException e) {
			MQConfig.MQHOST = "error";
			e.printStackTrace();
		}
		SpringApplication.run(GobiiconnectorApplication.class, args);
	}

	public static JSONObject parseConfig(String confPath) {
		JSONObject json = new JSONObject();
		try {
			File myObj = new File(confPath);
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				String[] line = data.split("=", 0);
				if(line.length > 0) {
					json.put(line[0], line[1]);
				}
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
}
