let gobii = {
    stompClient : null,

    callback: function(data) { console.log(data); },

    listener: null,

    connect: function(queueId) {
        var socket = new SockJS('/stream-updates');
        gobii.stompClient = Stomp.over(socket);
        gobii.stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            gobii.stompClient.subscribe('/queue/'+queueId, function (payload) {
                let rawMsg = JSON.parse(payload.body);
                let cleanMsg = "waiting"
                try {
                    cleanMsg = JSON.parse(rawMsg.name);
                    if(cleanMsg.progress == "done") {
                        clearInterval(gobii.listener);
                        gobii.listener = null;
                    }
                    gobii.callback(cleanMsg);
                } catch(e) {
                    console.log(rawMsg);
                }
            });
        });
        gobii.ping(queueId);
    },

    disconnect: function() {
        if (gobii.stompClient !== null) {
            gobii.stompClient.disconnect();
        }
        setConnected(false);
        console.log("Disconnected");
    },

    ping: function(queueId) {
        gobii.listener = setInterval(function() {
            gobii.stompClient.send("/ping/"+queueId, {}, JSON.stringify({'name': queueId}));
        }, 1000);
    },

    load: function(host, param, callback=null) {
        fetch(host+'/api/v1/job', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(param)
        })
        .then(response => response.json())
        .then(data => {
            let qId = data[0].split("/");
            qId = qId[qId.length-1];
            gobii.connect(qId);
        });
        if(callback)
            gobii.callback = callback;
    },
}