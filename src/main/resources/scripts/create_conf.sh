if [ $# -lt 1 ]
then
    echo "Usage: create_conf.sh <MQ Hostname>"
    echo "Creates connector.conf file in the local directory at time of calling"
    exit
fi
FILE="./connector.conf"
rm -f $FILE
touch $FILE
echo "GBLOADERPATH=/gobii_bundle/loaders/intertek-csv/load_csv.sh" >> $FILE
echo "RMQHOST="$1 >> $FILE
